/* Copyright 2003 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-*-C-*-
 *
 * Types and functions for Templates
 */

#define VALID_BUFFER_LEN 512

typedef enum
{
    AddHighlight = 1,
    RemoveHighlight = -1
} AdjustType;

extern error * template_load_prototypes (void);
extern char * template_validation_command_start (char *valid, char command);
extern char * template_validation_command_end (char *start);
extern void template_edit_validation (char *vstring, char *command, char *new, char *old);
extern char * template_get_validation (IconPtr icon);
extern void template_read_validation (IconPtr icon, char *buf);
extern void template_write_validation (IconPtr icon, char *buf);
extern Bool template_read_icon_name (IconPtr icon, char *buf);
extern void template_write_text (IconPtr icon, char *buf, int buflen);
extern char *template_control_to_null (char *str);
extern error * template_merge (char *buf, int size, ResourcePtr res, RectPtr bbox, Bool nudge);
extern error * template_load (char *buf, int size, DocumentPtr doc);
extern error * template_load_file (char *filename, DocumentPtr doc, ResourcePtr res, Bool nudge);
extern error * template_free_icon (IconInfoPtr icon, int num);
extern error * template_delete_selection (ResourcePtr res);
extern error * template_close_window (ResourcePtr res);
extern error * template_free (ResourcePtr res);
extern error * template_open_window (WindowPtr win, ResourcePtr doc);
extern void template_item_bbox (ResourcePtr res, ItemInfoPtr item, RectPtr bbox);
extern void template_adjust_item_bbox (AdjustType way, RectPtr src, RectPtr dst);
extern error * template_redraw_window (WindowRedrawPtr redraw, ResourcePtr res);
extern error * template_lose_selection (ResourcePtr res);
extern error * template_claim_drag (ResourcePtr res,             /* the resource in this window */
                                    int windowhandle,            /* window handle of receiver/claimant */
                                    MessageDraggingPtr msg,      /* may contain different window handle */
                                    int *claimref);              /* update with new myref, else 0 */
extern error * template_mouse_click (MouseClickPtr mouse, unsigned int modifiers, ResourcePtr res);
extern int template_file_size (DocumentPtr doc, ResourcePtr res, Bool selection);
extern error * template_save_file (DocumentPtr doc, ResourcePtr res, char *filename, Bool selection, PointPtr offset);
extern error * template_save_to_memory (DocumentPtr doc, ResourcePtr res, char *buffer, Bool selection, PointPtr offset);
extern error * template_renumber_icon (ResourcePtr res, ItemInfoPtr item, int old, int new);
extern void template_icon_size (IconPtr icon, int *w, int *h);
extern error * template_key_pressed (ResourcePtr res, KeyPressPtr key, Bool *consumed);
extern error * template_refind_fonts (void);
