/* Copyright 2003 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-*-C-*-*
 * Error handling for ResEd
 */

#include "resed.h"
#include <stdarg.h>

static error err = {0, ""};


/*
 * Return error message of the given text.  Should usually use
 * error_lookup instead.
 */

error * error_message (char *format, ...)
{
    va_list list;
    va_start (list, format);
    vsprintf (err.errmess, format, list);
    va_end (list);
    err.errnum = 1;
    return &err;
}


/*
 * Return error message of the given looked-up text
 */

error * error_lookup (char *tag, ...)
{
    va_list list;
    char *format = message_lookup(&msgs, tag);
    va_start (list, tag);
    vsprintf (err.errmess, format, list);
    va_end (list);
    err.errnum = 1;
    return &err;
}


/*
 * Display non-fatal WIMP error box
 */

void error_box (error *e)
{
    char *tag = "TaskName";
    char *taskname = message_lookup(&msgs, tag);
    if (taskname == tag)
        taskname = ICONNAME;                 /* fallback */

    _swix(Wimp_ReportError, _INR(0,2),
          e, BIT(4), taskname);
}


/*
 * Display fatal error and then exit, unless e == NULL, in which
 * case just exit.
 */

void error_exit (error *e)
{
    char *tag = "TaskName";
    char *taskname = message_lookup(&msgs, tag);
    if (taskname == tag)
        taskname = ICONNAME;                 /* fallback */

    if (wimpstarted)
    {
        if (e)                               /* if this fails, blow up */
            _swi(Wimp_ReportError, _INR(0,2),
                 e, 0, taskname);
        document_free_all ();
        checkheap(TRUE);

        block
        {
            int task;
            strncpy((char *)&task, "TASK", 4);
            _swi(Wimp_CloseDown, _INR(0,1),
                 taskhandle, task);
        }
    }
    else
    {
        if (e)
            fprintf (stderr, "%s: %s\n", taskname, e->errmess);
    }
dprintf("\nWE'RE GONE");
    exit (e ? e->errnum : 0);
}
